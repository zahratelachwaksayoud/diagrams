# PostgreSQL Hot Standby with 2 nodes

## Hot Standby 2 : Stable State

```mermaid
graph TD
  app> fa:fa-cloud Application]
  pool( fa:fa-filter pgBouncer)
  1[ fa:fa-database Node A = Postgres Primary]
  2[ fa:fa-database Node B = Postgres Standby]
  app--SQL-->pool
  pool--SQL-->1
  pool-.fallback.->2
  1--streaming replication-->2
```


## Hot Standby 2 :Fail Over

```mermaid
graph TD
  app> fa:fa-cloud Application]
  pool( fa:fa-filter pgBouncer)
  1[ fa:fa-times-circle Node A = Stopped ]
  2[ fa:fa-database Node B = Postgres Standby]
  app--SQL-->pool
  pool--SQL-->2
  %%pool-.fallback.->2
  %%1--synchonous replication-->2
```

## Hot Standby 2 : Fail back


```mermaid
graph TD
  app> fa:fa-cloud Application]
  pool( fa:fa-filter pgBouncer)
  1[ fa:fa-database Node A = Postgres primary ]
  2[ fa:fa-database Node B = Postgres Standby ]
  app--SQL-->pool
  pool--SQL-->2
  pool-.fallback.->1
  2--synchonous replication-->1
```
