
DEST?=codeblocks

PANDOC?=pandoc 
MERMAID?=mermaid.cli

#MERMAID_OPT=-t neutral

# Use docker 
#MERMAID="docker run --rm --volume `pwd`/$(DEST):/data/$(DEST) jnewland/mermaid.cli"

##
## Step 1 : extract mermaid code
## This is where we generate the mmmd files
## 
SRC = $(wildcard *.md)
JSON = $(SRC:.md=.json)

.PHONY: mmd
mmd: $(JSON)

%.json: %.md
	$(PANDOC) --filter pandoc_extract_code $^ -o $@

##
## Step 2 : Build
##
MMD := $(shell find $(DEST) -name '*.md')
SVG = $(MMD:.md=.svg)
PNG = $(MMD:.md=.png)

.PHONY: png svg
png: $(PNG)
svg: $(SVG)
		
%.png: %.md
	$(MERMAID) $(MERMAID_OPT) -i $^ -o $@

%.svg: %.md
	$(MERMAID) $(MERMAID_OPT) -i $^ -o $@
	
##
## Step 3 : build an HTML index
##	
	
$(DEST)/index.html:
	tree -H '.' -L 1 --noreport --charset utf-8 $(DEST) > $@	
	
